1. 把範本檔案放置 temp 資料夾
2. 條碼計算 excel 放置跟 main.py 同目錄下
3. 使用虛擬環境 -- python 請先安裝 pipenv 套件

   - 進入終端機 啟用虛擬環境

   ```bash
   pipenv shell
   ```

   - 安裝所需套件

   ```bash
   pipenv install
   ```

   - 執行 main.py

   ```bash
   pipenv run python main.py
   ```

4. 產出檔案會在 output 底下

5. 針對客人為 NAUS0015 有進行不足 15 碼補齊尾端空白
