import pandas as pd
import os
import shutil


def main():
    df = pd.read_excel(
        '條碼需求計算1007.xlsm', 
        usecols=['Item', 'Material','客人'], 
        )

    result = df.to_dict(orient='records')

    # get temp folder all filename
    temp_files = os.listdir('./temp')


    for i in range(len(result)):
        material_name = result[i]['Material']
        if f'{material_name}.btw' in temp_files:
            # print(result[i]) # get there has temp
            if result[i]['客人'] == 'NAUS0015':
                item_name = result[i]['Item'].ljust(15)
                full_file_name = f'{item_name}_{material_name}'
                shutil.copy(f'./temp/{material_name}.btw',f'./output/{full_file_name}.btw')
            else:
                item_name = result[i]['Item']
                full_file_name = f'{item_name}_{material_name}'
                shutil.copy(f'./temp/{material_name}.btw',f'./output/{full_file_name}.btw')
        else:
            # print(f'{material_name} -> 沒有匹配的temp檔案')
            pass

if __name__ == '__main__':
    main()
    print('Finished, 請關閉視窗')
    input()